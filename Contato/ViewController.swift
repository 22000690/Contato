//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let endereco: String
    let email: String
    
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos: [Contato] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula" , for:indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.endereco.text = contato.endereco
        cell .email.text = contato.email
        
        return cell
        
    }
    
    
    @IBOutlet weak var tableview: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        
        
        listaDeContatos.append(Contato(nome: "contato 1", numero: "3198-3257" , endereco: "Rua do tiguinha" , email: "contato1@gmail.com.br"))
        listaDeContatos.append(Contato(nome: "contato 2", numero: "3874-3226" , endereco: "Rua da esmeralda" , email: "contato2@gmail.com.br"))
        listaDeContatos.append(Contato(nome: "contato 3", numero: "3184-3807" , endereco: "Rua do jujuba" , email: "contato3@gmail.com.br"))
        
        
    
        
    
        
    
        
    
        
        
    
    }


}

